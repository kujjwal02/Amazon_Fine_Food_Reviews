# Sentiment Analysis on Amazon Fine Food Reviews

The Amazon Fine Food Reviews dataset consists of reviews of fine foods from Amazon.

Data Source: https://www.kaggle.com/snap/amazon-fine-food-reviews

#### Objective:
1. Given a review, determine whether the review is positive (Rating of 4 or 5) or negative (rating of 1 or 2).
2. Learn and experiment new techniques, algorithms or library in the process.

[View Notebook](http://bit.ly/AmznSentimentClassifier)